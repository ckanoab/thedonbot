import plugins, base64


def _initialise(bot):
    plugins.register_user_command(["b64"]); 
    plugins.register_user_command(["rot"]); 
    plugins.register_user_command(["rev"]); 
    plugins.register_user_command(["binary"]); 

def rot_alpha(n):
    from string import ascii_lowercase as lc, ascii_uppercase as uc
    lookup = str.maketrans(lc + uc, lc[n:] + lc[:n] + uc[n:] + uc[:n])
    return lambda s: s.translate(lookup)


def b64(bot, event, *args):
     if not args[0]: 
         yield from bot.coro_send_message(event.conv, "<b>Syntax:</b> <i>string</i>")
     else:
        string = ' '.join(list(args))
        try: 
            encoded = base64.b64encode(bytes(string, 'utf-8'))
            yield from bot.coro_send_message(event.conv, "Encode: " + str(encoded, 'utf-8'))
        except TypeError:
            yield from bot.coro_send_message(event.conv, "Encode failure: " + TypeError)
        try:
            decoded = base64.b64decode(bytes(string, 'utf-8'))
            yield from bot.coro_send_message(event.conv, "Decode: " + str(decoded, 'utf-8'))
        except TypeError:
            yield from bot.coro_send_message(event.conv, "Decode Failure: " + TypeError)



def rot(bot, event, *args):
    if not args[0]: 
         yield from bot.coro_send_message(event.conv, "<b>Syntax:</b>rot <i>string</i>")
    else:
        #try:
            string = ' '.join(list(args))
            response = "" 
            for x in range(1, 26):
                #response += "rot-" + str(x, 'utf-8') + " : " + 
                response += "<b>rot-%d </b>: " % x
                response += rot_alpha(x)(string) 
                response += "\n"

            yield from bot.coro_send_message(event.conv, response)
        #except TypeError:
        #    yield from bot.coro_send_message(event.conv, "Encode failure: " + TypeError)

def rev(bot, event, *args):
    if not args[0]: 
         yield from bot.coro_send_message(event.conv, "<b>Syntax:</b>rev <i>string</i>")
    else:
        string = ' '.join(list(args))
        response = "<b>Reversed</b> : <i>" + string[::-1] + "</i>"
        yield from bot.coro_send_message(event.conv, response)

def binary(bot, event, *args):
    if not args[0]: 
         yield from bot.coro_send_message(event.conv, "<b>Syntax:</b>binary <i>string</i>")
    else:
        string = ' '.join(list(args))
        response = ' '.join(format(ord(x), 'b') for x in string)
        yield from bot.coro_send_message(event.conv, response)
